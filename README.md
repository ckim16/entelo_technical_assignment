# README #

### What is this repository for? ###
Entelo Technical Assignment from Cheewoon Kim

### How do I get set up? ###

Clone it 

npm install 

npm start(local server) 

npm run dev(webpack for front-end)

Note: If you would like to run this project locally, you have to set config.js file in the server and

create an object(see below) that contains your email(I used my gmail account) and password to set a quick SMTP. 

You might have to allow access to low secured apps.

-------config.js(example)

let config = {};

config.username = "your_email@example.com";

config.password = "password for your email account";

module.exports = config;


### Who do I talk to? ###

aero.cheewoon@gmail.com