import axios from 'axios';

import { SEND_EMAIL } from './types';

const base_url = 'http://localhost:3000';

export function sendAnEmail({ to, subject, content }) {
  return function(dispatch) {
    axios.post(`${base_url}/send`, { to, subject, body: content })
    .then(res => {
      dispatch({ 
        type: SEND_EMAIL, 
        payload: true  
      });
    })
    .catch(err => console.log('send an email err', err));
  };
}