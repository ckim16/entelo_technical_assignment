import React, { Component } from 'react';

import Header from './header';
import Email from './email';

export default class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Email/>
      </div>
    );
  }
}