import React, { Component } from 'react';
import { connect } from 'react-redux';

import { sendAnEmail } from '../actions/index.js';

class Email extends Component {
  constructor(props) {
    super(props);

    this.state = {
      to: '',
      subject: '',
      content: ''
    };

    this.onToAddrChange = this.onToAddrChange.bind(this);
    this.onSubjectChange = this.onSubjectChange.bind(this);
    this.onContentChange = this.onContentChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.sent !== prevProps.sent) {
      alert(`Email has been sent to ${this.state.to}`);
      this.setState({ to: '', subject: '', content: '' });
    }
  }

  onToAddrChange(e) {
    this.setState({ to: e.target.value });
  }

  onSubjectChange(e) {
    this.setState({ subject: e.target.value });
  }

  onContentChange(e) {
    this.setState({ content: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.to === '') {
      alert('Email address is required!');
      return;
    }
    this.props.sendAnEmail({
      to: this.state.to,
      subject: this.state.subject,
      content: this.state.content
    });
  }

  render() {
    return (
      <div className="form">
        <form onSubmit={this.handleSubmit} className="form-group">
          <label>
            To:<br/>
            <input
              className="to"
              value={this.state.to}
              onChange={this.onToAddrChange}
            />
          </label><br/>
          <label>
            Subject:<br/>
            <input
              className="subject"
              value={this.state.subject}
              onChange={this.onSubjectChange}
            />
          </label><br/>
          <textarea
            className="content"
            value={this.state.content}
            onChange={this.onContentChange}
          /><br/>
          <button className="btn btn-success">
            SEND{' '}
            <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
          </button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    sent: state.email
  };
}

function mapDisPatchToProps(dispatch) {
  return {
    sendAnEmail: ({ to, subject, content }) => dispatch(sendAnEmail({ to, subject, content }))
  };
}

export default connect(mapStateToProps, mapDisPatchToProps)(Email);

