import { SEND_EMAIL } from '../actions/types';

export function email(state=false, action) {
  if (action.type === SEND_EMAIL) {
    return action.payload;
  } 
  return state;
}