import { combineReducers } from 'redux';

import { email } from './email_reducer';

const rootReducer = combineReducers({
  email
});

export default rootReducer;