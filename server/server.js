const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const path = require('path');
const email = require('emailjs');

let config = require('./config');

const server  = email.server.connect({
   user: config.username, 
   password: config.password, 
   host: 'smtp.gmail.com', 
   ssl: true
});

app.use(bodyParser.json({type: '*/*'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(function crossOrigin(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  return next();
});

app.use(express.static(path.resolve(__dirname, '../dist/')));

app.post('/send', (req, res, next) => {
  const { body } = req;
  server.send({
    from: "test@gmail.com", 
    to: req.body.to,
    subject: req.body.subject,
    text: req.body.body, 
  }, function(err, message) {
    if (err) throw err;
    console.log('Email was sent: ' + body);
    res.sendStatus(200);
  });
});

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, '../client/index.html'));
});

app.listen(port, () => {
  console.log('Listening on port', port);
});